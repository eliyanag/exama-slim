<?php

require "bootstrap.php";
require "vendor/autoload.php";

use ExamA\Models\Product;
use ExamA\Middleware\Logging;


$app = new \Slim\App();
$app->add(new Logging());

//hello name---------------------------------------------------------------------------------------------------------
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});


//get all products---------------------------------------------------------------------------------------------------------
$app->get('/products', function($request, $response, $args){
    $_product = new Product(); //create new product
    $products = $_product->all();
    $payload = [];
    foreach($products as $product){
        $payload[$product->id] = [
            'name'=>$product->name,
            'price'=>$product->price
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});


//get search---------------------------------------------------------------------------------------------------------
$app->get('/search/{name}', function($request, $response,$args){
    $_name = $args['name'];
    $_product = new Product(); //create new product
    $products = $_product->all();
    $payload = [];
    foreach($products as $product){
        if($product->name == $_name){
            $payload[$product->id] = [
                'name'=>$product->name,
                'price'=>$product->price
            ];
        }
    }
    return $response->withStatus(200)->withJson($payload);
});
//---------------------------------------------------------------------------------------------------------------
$app->get('/users/{id}', function($request, $response, $args){
    $_id = $args['id'];
    $user = User::find($_id);
       return $response->withStatus(200)->withJson($user);
    });
//---------------------------------------------------------------------------------------------------------------
$app->get('/products/{id}', function($request, $response, $args){
    $_id = $args['id'];
    $product = Product::find($_id);
       return $response->withStatus(200)->withJson($product);
    });
//update----------------------------------------------------------------------------------------------------------------------
$app->put('/products/{id}', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name','');
    $price = $request->getParsedBodyParam('price','');
        $_product = Product::find($args['id']);
        $_product->name = $name;
        $_product->price = $price;

        if($_product->save()){ //אם הערך נשמר
            $payload = ['id' => $_product->id,"result" => "The user has been updates successfuly"];
            return $response->withStatus(200)->withJson($payload);
        }  
    else{
            return $response->withStatus(400);
        } 
});
//Login without JWT
$app->post('/login', function($request, $response,$args){
    $name  = $request->getParsedBodyParam('name','');
    $price = $request->getParsedBodyParam('price','');    
    $_product = Product::where('name', '=', $name)->where('price', '=', $price)->get();
    
    if($_product[0]->id){
        $payload = ['success'=>true];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }
});
    

//אבטחת מידע--------------------------------------------------------------------------------------------------------
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();